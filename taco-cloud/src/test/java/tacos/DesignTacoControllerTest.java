package tacos;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import tacos.controllers.DesignTacoController;


@WebMvcTest(DesignTacoController.class)
public class DesignTacoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testDesignTacoPage() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/design"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("design"))
                .andExpect(
                        MockMvcResultMatchers.content().string(
                                Matchers.containsString("Design your Taco!")));
    }

    @Test
    void testDesignTacoSubmit() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/design")
                .param("name", "Taco Name")
                .param("ingredients", "FLTO", "TMTO", "JACK", "SRCR"))
                .andExpect(MockMvcResultMatchers.status().is3xxRedirection());
    }

    @Test
    void testInvalidValuesDesignTacoSubmit() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/design")
                .param("name", "")
                .param("ingredients", "FLTO", "TMTO", "JACK", "SRCR"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
