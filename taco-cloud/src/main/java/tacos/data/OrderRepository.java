package tacos.data;

import org.springframework.data.repository.CrudRepository;
import tacos.domain.entities.Order;

public interface OrderRepository extends CrudRepository<Order, Long> {

}
