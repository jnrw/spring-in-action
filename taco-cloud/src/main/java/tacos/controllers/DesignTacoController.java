package tacos.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import tacos.data.IngredientRepository;
import tacos.data.TacoRepository;
import tacos.domain.entities.Order;
import tacos.domain.entities.Taco;
import tacos.domain.service.DesignTacoService;

import javax.validation.Valid;
import java.util.logging.Logger;

@Controller
@RequestMapping("/design")
@SessionAttributes("order")
public class DesignTacoController {

    private static Logger log = Logger.getLogger(DesignTacoController.class.getName());

    private final DesignTacoService designTacoService;
    private final IngredientRepository ingredientRepository;
    private final TacoRepository tacoRepository;

    @Autowired
    public DesignTacoController(DesignTacoService designTacoService, IngredientRepository ingredientRepository, TacoRepository tacoRepository) {
        this.designTacoService = designTacoService;
        this.ingredientRepository = ingredientRepository;
        this.tacoRepository = tacoRepository;
    }

    @ModelAttribute(name = "order")
    public Order order() {
        return new Order();
    }

    @ModelAttribute(name = "design")
    public Taco design() {
        return new Taco();
    }

    @GetMapping
    public String showDesignForm(Model model) {
        log.info("Getting /design");
        model.addAttribute("mapIngredients", this.designTacoService.getIngredientsByType());
        model.addAttribute("taco", new Taco());
        return "design";
    }

    @PostMapping
    public String processDesign(@Valid Taco taco, Errors errors, Model model, @ModelAttribute Order order) {
        log.info("Processing " + taco.toString());
        if (errors.hasErrors()) {
            model.addAttribute("mapIngredients", this.designTacoService.getIngredientsByType());
            return "design";
        }

        Taco saved = tacoRepository.save(taco);
        order.getTacos().add(saved);
        return "redirect:/orders/current";
    }
}

