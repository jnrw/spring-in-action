package tacos.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tacos.data.IngredientRepository;
import tacos.domain.entities.Ingredient;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@Service
public class DesignTacoService {

    private final IngredientRepository ingredientRepository;

    @Autowired
    public DesignTacoService(IngredientRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }


    public Map<Ingredient.Type, List<Ingredient>> getIngredientsByType() {
        var result = new TreeMap<Ingredient.Type, List<Ingredient>>();
        this.ingredientRepository.findAll().forEach(ingredient -> {
            if (result.containsKey(ingredient.getType())) {
                result.get(ingredient.getType()).add(ingredient);
            } else {
                var ingredients = new ArrayList<Ingredient>();
                ingredients.add(ingredient);
                result.put(ingredient.getType(), ingredients);
            }
        });
        return result;
    }
}
